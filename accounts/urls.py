from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from accounts.views import user_create, profile
from django.conf import settings
from django.conf.urls.static import static
from accounts.views import ChangePasswordView


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", user_create, name="signup"),
    path("profile/", profile, name="users-profile"),
    path(
        "change-password/",
        ChangePasswordView.as_view(),
        name="change-password",
    ),
]
