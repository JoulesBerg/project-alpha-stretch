How to do a merge:

in your feature commit and push your feature branch

Navigate to your personal review branch

Pull main branch to your review branch 
(This makes sure you have the most up to date version of the group project on your review branch, because other people will be pushing to main)

git pull origin main

Merge your feature branch to the review branch

git merge Featurebranch

Fix merge issues locally(this will usually be variable names, context data conflicts, etc.)

Once all issues are fixed make a merge request on gitlab with your review branch as the first option and the branch you are merging to as the second option. 
Assign a reviewer 
Wait

