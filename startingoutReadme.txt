How to get started with gitlab workflow:

REQUEST ACCESS TO THE GITLAB, LINK IN THE SLACK CHANNEL

Clone it to your desired directory

make a personal review branch using this command (we will use this branch later when merging your code with everyone elses)

git checkout -b Yourname-Review

navigate to that branch with

git checkout Yourname-Review

create a feature branch under your personal review branch to start a feature

git checkout -b Featurename

navigate to the feature branch

git checkout Featurename

Start adding or working on your feature! We will talk about how to merge more later




