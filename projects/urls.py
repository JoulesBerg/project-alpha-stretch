from django.urls import path
from django.views.generic import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage

from projects.views import (
    ProjectListView,
    ProjectCreateView,
    view_project_details,
    SearchListView,
    ProjectUpdateView,
    ProjectDeleteView
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>", view_project_details, name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path("search/", SearchListView.as_view(), name="search_results"),
    path("<int:pk>/edit/", ProjectUpdateView.as_view(), name="edit_project"),
    path(
        "<int:pk>/delete/", ProjectDeleteView.as_view(), name="delete_project"
    ),
    path(
        "favicon.ico",
        RedirectView.as_view(url=staticfiles_storage.url("favicon.ico")),
    )
]
