from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from django.db.models import Q
from .models import Project, Message
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from projects.models import Project
from django.contrib.auth.models import User

# Create your views here.

class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = "projects"
    template_name = "projects/projectlist.html"
    paginate_by = 10

    def get_context_data(
        self,
    ):  # re-define get context data so that pagination and search work together instead of needing to choose between one or the other.
        context = super().get_context_data()
        query = self.request.GET.get("q")
        users = User.objects.all()
        if not query:
            query = ""
        context[
            "query"
        ] = query  # adds the query from the search box (name q)as a context variable that can be accessed in list.html

        context["users"] = users
        return context

    def get_queryset(
        self,
    ):  # re-defining get_queryset to work with search functionality
        user = self.request.user

        query = self.request.GET.get(
            "q"
        )  # q is the name variable defined in the HTML input function in list.html
        if not query:
            query = ""  # ensures that query is not None, as searching for a string that contains None using the filter function below causes an error
        return Project.objects.filter(
            Q(name__icontains=query), Q(members=user) | Q(host=user)
        ).distinct()

        
@login_required
def view_project_details(request, pk):
    project = Project.objects.get(id=pk)
    project_messages = project.message_set.all().order_by('-created')
    participants = project.members.all()

    if request.method == "POST":
        message = Message.objects.create(user=request.user, project=project, body=request.POST.get('body'))
        return redirect('show_project', pk=project.pk)
    
    context = {'project': project, 'project_messages': project_messages, "participants": participants}
    return render(request, 'projects/projectdetail.html', context)


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/projectcreate.html"
    fields = [
        "name",
        "description",
        "members",
    ]

    def form_valid(self, form):
        new = form.save(commit=False)
        new.host = self.request.user
        new.save()
        form.save_m2m()
        return redirect("show_project", pk=new.id)


class SearchListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "search/search_results.html"

    def get_queryset(self):
        query = self.request.GET.get("q")
        object_list = Project.objects.filter(
            Q(name__icontains=query) | Q(description__icontains=query)
        )
        return object_list


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    template_name = "projects/projectedit.html"
    fields = [
        "name",
        "description",
        "members",
    ]

    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})


class ProjectDeleteView(LoginRequiredMixin, DeleteView):
    model = Project
    template_name = "projects/projectdelete.html"
    success_url = reverse_lazy("list_projects")
