
Once a merge has been requested it will appear on the left hand toolbar of gitlab. Open merge requests and view the merge request you have been assigned

to review the code make sure any changes you are working on on your local machine are saved and PUSHED to a personal feature branch before you start reviewing.

To start reviewing click the blue "Code" button and follow the instructions on checkout branch to get the branch onto your local machine.

Do what you need to do to check the functionality of the features: migrate, runserver, etc.

If everything works approve the merge request and click the blue "merge" button on the gitlab site.

You're all done!
