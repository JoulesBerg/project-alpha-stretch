from django.forms import ModelForm, DateInput, BooleanField
from .models import Task

# from models import Task


# class CustomDateInput(DateInput):
#     input_type = "date"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": DateInput(attrs={"type": "date"}),
            "due_date": DateInput(attrs={"type": "date"}),
        }
