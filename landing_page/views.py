from django.shortcuts import render
from django.contrib.auth.models import User

def view_landing_page(request):

  user = request.user
  print(user)

  context = {
    'user': user
  }

  return render(request, 'landing_page/home.html', context)
